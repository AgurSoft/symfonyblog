function deleteComment(id) {

    $.ajax({
        type: 'POST',
        url: '/comment/delete/' + id,
    }).done(function (data) {
        console.log(data);
        $('#' + id).hide();
    });
}

function prepareAuthor(author) {
    author = author.split(' ');
    var array = [];
    author.forEach(function (item, i, arr) {
        array.push(item.charAt(0).toUpperCase() + item.substr(1));
    });
    if (array.length < 2) {
        alert('Поле "Автор Коментария" должно состоять минимум из 2-х слов');
        return 'false';
    }
    else {
        return array.join(' ');
    }

}

$(document).ready(function () {
    $('form').submit(function (event) {
        event.preventDefault();
        var author = $('input[name=author]').val();
        var author = prepareAuthor(author);
        if (author === 'false') {
            return false;
        }
        var formData = {
            'author': author,
            'comment_content': $('input[name=comment_content]').val(),
            'post': $('input[name=post]').val()
        };
        console.log(formData);
        $.ajax({
            type: 'post',
            url: '/comment/create',
            data: formData,
            dataType: 'json'
        }).done(function (data) {
            console.log(data);
            $('#comments').append('<div id="' + data.id + '">' +
                '<hr>' +
                '<h4 >' + data.author + '</h4>' +
                '<p>' + data.content +
                '</p>' +
                '<input type="button" onclick="deleteComment(' +
                data.id +
                ')" class="btn btn-danger" value="Удалить">' +
                '</div>' +
                '<hr>' +
                '</div>');
        });
    });
});
