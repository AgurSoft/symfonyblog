<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends Controller
{
    /**
     * @Route("/categories/{category}",name="category.show",methods={"GET"}))
     */
    public function show($category)
    {
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->findOneBy([
                'name' => $category
            ]);

        return $this->redirectToRoute('posts',['category'=>$category->getId()]);
    }

    /**
     * @Route("/category/create", name="category.create",methods={"GET"})
     */
    public function create()
    {
        $category = new Category();

        $form = $this->createFormBuilder($category)
            ->setAction($this->generateUrl('category.store'))
            ->add('name', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('description', TextType::class, [
                'attr' => [
                    'class' => 'form-control form-group'

                ]
            ])
            ->add('save', SubmitType::class, [
                'label' => "Create Category",
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ])
            ->getForm();

        return $this->render('category/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/category/store", name="category.store",methods={"POST"})
     */
    public function store(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $request->request->get('form');
        $category = new Category();
        $category->setName($form['name']);
        $category->setDescription($form['description']);
        $em->persist($category);
        $em->flush();
        return $this->redirectToRoute('posts');
    }
}
