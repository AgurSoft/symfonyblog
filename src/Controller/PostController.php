<?php

namespace App\Controller;

use App\Entity\Category;
use App\Entity\Comment;
use App\Entity\Post;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\Exception\UploadException;
use Symfony\Component\HttpFoundation\Request;

class PostController extends Controller
{
    /**
     * @Route("/", name="posts")
     */
    public function index(Request $request)
    {
        if ($request->get('category')) {
            $category_id = $request->get('category');
            $posts = $this->getDoctrine()->getRepository(Post::class)->findBy(['category_id' => $category_id]);
        } else {
            $posts = $this->getDoctrine()->getRepository(Post::class)->findAll();
        }

        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();

        return $this->render('post/index.html.twig', [
            'categories' => $categories,
            'posts' => $posts,
        ]);
    }

    /**
     * @Route("/post/{post}", name="post.show")
     */
    public function show(Post $post)
    {
        $comments = $this->getDoctrine()->getRepository(Comment::class)->findBy(['post_id' => $post->getId()]);
        return $this->render('post/show.html.twig', [
            'post' => $post,
            'comments'=>$comments
        ]);
    }

    /**
     * @Route("/posts/create", name="posts.create")
     */
    public function create()
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        $post = new Post();
        $form = $this->createFormBuilder($post)
            ->setAction($this->generateUrl('posts.store'))
            ->add('title', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('content', TextType::class, [
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('category_id', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name'
            ])
            ->add('file', FileType::class, [
            ])
            ->add('save', SubmitType::class, [
                'label' => "Create Category",
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ])
            ->getForm();
        return $this->render('post/create.html.twig', [
            'form' => $form->createView(),
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/posts/store",name="posts.store",methods={"POST"})
     */
    public function store(Request $request)
    {
        $data = $request->get('form');
        $file = $request->files->get('form');
        $file = $file['file'];
        $size = $file->getClientSize();
        if ($size > 2000000) {
            throw new UploadException('Размер файла слишком большой');
        }
        $fileName = md5(uniqid()) . '.' . $file->guessExtension();
        $file->move($this->getParameter('images_directory'), $fileName);

        $post = new Post();
        $post->setTitle($data['title']);
        $post->setContent($data['content']);
        $post->setCategoryId($data['category_id']);
        $post->setFile($fileName);

        $em = $this->getDoctrine()->getManager();
        $em->persist($post);
        $em->flush();
        return $this->redirectToRoute('posts');
    }

    /**
     * @Route("/post/{post}/edit",name="posts.edit")
     */
    public function edit(Post $post)
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        $category_id = $post->getCategoryId();
        return $this->render('post/edit.html.twig', [
            'post' => $post,
            'categories' => $categories,
            'category_id' => $category_id
        ]);
    }

    /**
     * @Route("/posts/updating",name="posts.updating")
     */
    public function update(Request $request)
    {
        $data = $request->request->all();
        $post = $this->getDoctrine()
            ->getRepository(Post::class)
            ->find($data['id']);

        $post->setTitle($data['title']);
        $post->setContent($data['post_content']);
        $post->setCategoryId($data['category_id']);

        if ($request->files->get('file') != null) {
            $file = $request->files->get('file');
            $size = $file->getClientSize();
            if ($size > 2000000) {
                throw new UploadException('Размер файла слишком большой');
            }
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();
            $file->move($this->getParameter('images_directory'), $fileName);

            $post->setFile($fileName);
        }

        $em = $this->getDoctrine()->getManager();

        $em->persist($post);
        $em->flush();

        return $this->redirectToRoute('posts');
    }

    /**
     * @Route("/post/{post}/delete" ,name="posts.delete")
     */
    public function delete(Post $post)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        return $this->redirectToRoute('posts');
    }
}
