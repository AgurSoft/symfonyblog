<?php

namespace App\Controller;

use App\Entity\Comment;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CommentController extends Controller
{
    /**
     * @Route("/comment/create", name="comment.create",methods={"POST","GET"})
     */
    public function index(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $comment = new Comment();

        $comment->setAuthor($request->get('author'));
        $comment->setContent($request->get('comment_content'));
        $comment->setPostId($request->get('post'));

        $em->persist($comment);
        $em->flush();

        return JsonResponse::create([
            'id' => $comment->getId(),
            'author' => $comment->getAuthor(),
            'content' => $comment->getContent(),
            'post_id' => $comment->getPostId()
        ], 200);
    }

    /**
     * @Route("/comment/delete/{comment}",name="comments.delete",methods={"POST","GET","DELETE"})
     */
    public function delete(Comment $comment)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($comment);
        $em->flush();
        return JsonResponse::create(['status' => 'deleted'], '200');
    }
}
